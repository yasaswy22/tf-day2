
module "ec2" {
  source = "./terraform/modules/ec2"

  ec2_key_name = var.key_name
  ec2_public_key = var.public_key
  ec2_instance_type = var.instance_type
  ec2_sgs = ["${module.sg.sg_id}"]


}

module "sg" {
    source = "./terraform/modules/sg"
}