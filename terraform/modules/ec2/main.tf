provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAW77HYQCVM7UYUDGV"
  secret_key = "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"
}

resource "aws_key_pair" "ec2-access-demo" {
  key_name   = var.ec2_key_name
  public_key = var.ec2_public_key
}

data "aws_ami" "linux_ami_prac" {
  owners      = ["137112412989"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_instance" "ec2-demo" {
  ami                    = data.aws_ami.linux_ami_prac.id
  instance_type          = var.ec2_instance_type
  key_name               = aws_key_pair.ec2-access-demo.key_name
  vpc_security_group_ids = var.ec2_sgs
  tags = {
    Name = "demo-tf"
  }
}