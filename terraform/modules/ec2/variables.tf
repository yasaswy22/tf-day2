variable "ec2_key_name" {
}

variable "ec2_public_key" {
}

variable "ec2_instance_type" {
}

variable "ec2_sgs" {
  type = list(string)
}