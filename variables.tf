variable "key_name" {
  default = "rsa-key-20221025"
}

variable "public_key" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiM1kiRigSwtxhrR2I05hfbDGBPEsdXTPwndePoj18rtIwKVvWNafiRz6zdbdBH9/3e8wU6fXmJQNIx0y+q/ICFEDQ0YTKQQe2BjPlgUZSlbGm7VV+A/pCAfVLbhl4U8zI6r2IEq38IVhpoY+O5PlyDXAzZbrLwQvmH4mHzDlDkkeMxSXMnRgNEGWUG+/sRan566sRqhSnh0DsJ3xsVUMumoOKuQNnZuN/zAljFOgjg8kB5rSOfz80eW9b8BNp5cj0OxNhk/U7UD4a3DKFnrdVjNnk+r8Y1FE0Z+utCeOlSujV1Sxw3sEMq4ma/4nSzno2NcO0tSPGlFgVuhzTOdLR rsa-key-20221025"
}

variable "instance_type" {
  default = "t2.micro"
}